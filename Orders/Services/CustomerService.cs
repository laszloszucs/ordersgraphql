﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Orders.Models;
using Orders.Services.Interfaces;

namespace Orders.Services
{
    public class CustomerService : ICustomerService
    {
        private List<Customer> _customers = new List<Customer>
        {
            new Customer(1,"KinetEco"),
            new Customer(2, "Pixelford Photography"),
            new Customer(3, "Topsy Turvy"),
            new Customer(4, "Leaf & Mortar")
        };

        public Customer Get(int id)
        {
            return GetAsync(id).Result;
        }

        public Task<Customer> GetAsync(int id)
        {
            return Task.FromResult(_customers.FirstOrDefault(c => c.Id == id));
        }

        public Task<IEnumerable<Customer>> GetAsync()
        {
            return Task.FromResult(_customers.AsEnumerable());
        }
    }
}
