﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orders.Enums;
using Orders.Models;

namespace Orders.Services.Interfaces
{
    public interface IOrderService
    {
        Task<Order> GetAsync(int id);
        Task<IEnumerable<Order>> GetAsyncByOrderStatus(OrderStatus orderStatus);
        Task<IEnumerable<Order>> GetAsync();
        Task<Order> CreateAsync(OrderCreateInput order);
        Task<Order> StartAsync(int orderId);
    }
}