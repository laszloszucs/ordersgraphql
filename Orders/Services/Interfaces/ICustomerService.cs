﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Orders.Models;

namespace Orders.Services.Interfaces
{
    public interface ICustomerService
    {
        Customer Get(int id);
        Task<Customer> GetAsync(int id);
        Task<IEnumerable<Customer>> GetAsync();
    }
}