﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Orders.Enums;
using Orders.Models;
using Orders.Services.Interfaces;

namespace Orders.Services
{
    public class OrderService : IOrderService
    {
        public OrderService(IOrderEventService orderEventService)
        {
            _orderEventService = orderEventService;
        }

        private List<Order> _orders = new List<Order>
        {
            new Order(1,"1000", "1000 description",DateTime.Now,1),
            new Order(2,"2000", "2000 description",DateTime.Now.Subtract(new TimeSpan(2,0,0,0)),1),
            new Order(3,"3000", "3000 description",DateTime.Now.Subtract(new TimeSpan(3,0,0,0)),2),
            new Order(4,"4000", "4000 description",DateTime.Now.Subtract(new TimeSpan(4,0,0,0)),3),
            new Order(5,"5000", "5000 description",DateTime.Now.Subtract(new TimeSpan(5,0,0,0)),4),
        };
        private readonly IOrderEventService _orderEventService;

        public Task<Order> GetAsync(int id)
        {
            return Task.FromResult(_orders.FirstOrDefault(c => c.Id == id));
        }

        public Task<IEnumerable<Order>> GetAsyncByOrderStatus(OrderStatus orderStatus)
        {
            return Task.FromResult(_orders.Where(c => c.OrderStatus == orderStatus));
        }

        public Task<IEnumerable<Order>> GetAsync()
        {
            return Task.FromResult(_orders.AsEnumerable());
        }

        public Task<Order> CreateAsync(OrderCreateInput c)
        {
            var newId = _orders.Max(o => o.Id) + 1;
            var order = new Order(newId, c.Name, c.Description, c.Created, c.CustomerId);
            _orders.Add(order);

            _orderEventService.AddEvent(new OrderEvent(order));

            return Task.FromResult(order);
        }

        public Task<Order> StartAsync(int id)
        {
            var order = _orders.FirstOrDefault(o => o.Id == id);
            if (order == null)
            {
                return null;
            }

            order.Start();

            _orderEventService.AddEvent(new OrderEvent(order));

            return Task.FromResult(order);
        }
    }
}
