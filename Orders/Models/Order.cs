﻿using System;
using Orders.Enums;

namespace Orders.Models
{
    public class Order : OrderCreateInput
    {
        public int Id { get; }
        public OrderStatus OrderStatus { get; private set; }

        public Order(int id, string name, string description, DateTime created, int customerId)
        {
            Id = id;
            Name = name;
            Description = description;
            Created = created;
            CustomerId = customerId;
            OrderStatus = OrderStatus.CREATED;
        }

        public void Start()
        {
            OrderStatus = OrderStatus.PROCESSING;
        }
    }
}
