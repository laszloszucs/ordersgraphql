﻿using System;
using Orders.Enums;

namespace Orders.Models
{
    public class OrderEvent
    {
        public OrderEvent(Order order)
        {
            Id = Guid.NewGuid().ToString();
            OrderId = order.Id;
            Name = order.Name;
            OrderStatus = order.OrderStatus;
            TimeStamp = order.Created;
        }

        public string Id { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime TimeStamp { get; private set; }
    }
}
