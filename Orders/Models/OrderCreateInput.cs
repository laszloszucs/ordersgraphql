﻿using System;

namespace Orders.Models
{
    public class OrderCreateInput
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
    }
}
