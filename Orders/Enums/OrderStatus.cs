﻿using System;

namespace Orders.Enums
{
    [Flags] // bitként lehet kezelni az enumot
    public enum OrderStatus
    {
        CREATED = 2,
        PROCESSING = 4,
        COMPLETED = 8,
        CANCELLED = 16,
        CLOSED = 32
    }
}