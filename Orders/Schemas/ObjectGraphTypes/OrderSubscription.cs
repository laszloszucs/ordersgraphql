﻿using GraphQL.Types;
using GraphQL.Subscription;
using GraphQL.Resolvers;
using Orders.Enums;
using Orders.Models;
using Orders.Schemas.Types;
using Orders.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

namespace Orders.Schemas.ObjectGraphTypes
{
    public class OrderSubscription : ObjectGraphType<object>
    {
        private readonly IOrderEventService _orderEventService;

        public OrderSubscription(IOrderEventService orderEventService)
        {
            _orderEventService = orderEventService;
            Name = "Subscription";
            // Itt azért használunk AddField-et, mert elvileg a Field subscription esetén nincs támogatva.
            AddField(new EventStreamFieldType
            {
                Name = "orderEvent",
                Arguments = new QueryArguments(new QueryArgument<ListGraphType<OrderStatusEnumType>>
                {
                    Name = "statuses"
                }),
                Type = typeof(OrderEventType),
                Resolver = new FuncFieldResolver<OrderEvent>(ResolveEvent),
                Subscriber = new EventStreamResolver<OrderEvent>(Subscribe)
            });
        }

        private OrderEvent ResolveEvent(ResolveFieldContext arg)
        {
            return arg.Source as OrderEvent;
        }

        private IObservable<OrderEvent> Subscribe(ResolveEventStreamContext arg)
        {
            var statusList = arg.GetArgument<IList<OrderStatus>>("statuses", new List<OrderStatus>());

            if (statusList.Any())
            {
                OrderStatus statuses = 0;

                foreach (var status in statusList)
                {
                    statuses = statuses | status;
                }

                return _orderEventService.EventStream().Where(e => (e.OrderStatus & statuses) == e.OrderStatus);
            }
            else
            {
                return _orderEventService.EventStream();
            }
        }
    }
}
