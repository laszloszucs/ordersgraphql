﻿using GraphQL.Types;
using Orders.Enums;
using Orders.Schemas.Types;
using Orders.Services.Interfaces;

namespace Orders.Schemas.ObjectGraphTypes
{
    public class OrderQuery : ObjectGraphType
    {
        public OrderQuery(IOrderService orderService, ICustomerService customerService)
        {
            Field<OrderType>("order",
                arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
                resolve: o => orderService.GetAsync(o.GetArgument<int>("id")));

            Field<ListGraphType<OrderType>>("orderByStatus",
                arguments: new QueryArguments(new QueryArgument<OrderStatusEnumType> { Name = "orderStatus" }),
                resolve: o => orderService.GetAsyncByOrderStatus(o.GetArgument<OrderStatus>("orderStatus")));

            Field<ListGraphType<OrderType>>("orders", resolve: o => orderService.GetAsync());
            Field<ListGraphType<CustomerType>>("customers", resolve: o => customerService.GetAsync());
        }
    }
}