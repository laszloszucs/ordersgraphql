﻿using GraphQL.Types;
using Orders.Models;
using Orders.Schemas.Types;
using Orders.Services.Interfaces;

namespace Orders.Schemas.ObjectGraphTypes
{
    public class OrderMutation : ObjectGraphType
    {
        public OrderMutation(IOrderService orderService, ICustomerService customerService)
        {
            Field<OrderType>("createOrder",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<OrderCreateInputType>> {Name = "order"}),
                resolve: o => orderService.CreateAsync(o.GetArgument<OrderCreateInput>("order")));

            Field<OrderType>("startOrder",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                resolve: o => orderService.StartAsync(o.GetArgument<int>("id")));
        }
    }
}