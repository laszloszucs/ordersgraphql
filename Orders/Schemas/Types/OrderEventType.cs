﻿using GraphQL.Types;
using Orders.Models;

namespace Orders.Schemas.Types
{
    public class OrderEventType : ObjectGraphType<OrderEvent>
    {
        public OrderEventType()
        {
            Field(e => e.Id);
            Field(e => e.Name);
            Field(e => e.OrderId);
            Field<OrderStatusEnumType>("orderStatus", resolve: c => c.Source.OrderStatus);
            Field(e => e.TimeStamp);
        }
    }
}
