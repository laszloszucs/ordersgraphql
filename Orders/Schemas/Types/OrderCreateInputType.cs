﻿using GraphQL.Types;
using Orders.Models;

namespace Orders.Schemas.Types
{
    public class OrderCreateInputType : InputObjectGraphType<OrderCreateInput>
    {
        public OrderCreateInputType()
        {
            Field<NonNullGraphType<StringGraphType>>("name");
            Field<NonNullGraphType<StringGraphType>>("description");
            Field<NonNullGraphType<IntGraphType>>("customerId");
            Field<NonNullGraphType<DateGraphType>>("created");
        }
    }
}
