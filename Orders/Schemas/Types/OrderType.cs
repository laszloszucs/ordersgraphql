﻿using GraphQL.Types;
using Orders.Enums;
using Orders.Models;
using Orders.Services.Interfaces;

namespace Orders.Schemas.Types
{
    public class OrderType : ObjectGraphType<Order>
    {
        public OrderType(ICustomerService customerService)
        {
            Field(c => c.Id);
            Field(c => c.Name);
            Field(c => c.Description).Description("Leírás");
            Field<CustomerType>("customer", resolve: c => customerService.GetAsync(c.Source.CustomerId));
            Field(c => c.Created);
            Field<OrderStatusEnumType>("OrderStatus", "OrderStatus");
        }
    }
}
