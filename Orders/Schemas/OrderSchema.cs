﻿using GraphQL;
using GraphQL.Types;
using Orders.Schemas.ObjectGraphTypes;

namespace Orders.Schemas
{
    public class OrderSchema : Schema
    {
        public OrderSchema(OrderQuery orderQuery, OrderMutation orderMutation, OrderSubscription orderSubscription,
            IDependencyResolver dependencyResolver)
        {
            Query = orderQuery;
            Mutation = orderMutation;
            Subscription = orderSubscription;
            DependencyResolver = dependencyResolver;
        }
    }
}
