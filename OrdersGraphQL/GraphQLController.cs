﻿using Microsoft.AspNetCore.Mvc;

namespace OrdersGraphQL
{
    public class GraphQLController : Controller
    {
        public IActionResult Index()
        {
            return Redirect("/graphql");
        }
    }
}