using GraphiQl;
using GraphQL;
using GraphQL.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orders.Schemas;
using Orders.Schemas.Types;
using Orders.Services;
using Orders.Services.Interfaces;
using Orders.Schemas.ObjectGraphTypes;
using GraphQL.Server.Ui.Playground;

namespace OrdersGraphQL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services
                .AddSingleton<IOrderService, OrderService>()
                .AddSingleton<ICustomerService, CustomerService>()
                .AddSingleton<OrderType>()
                .AddSingleton<CustomerType>()
                .AddSingleton<OrderStatusEnumType>()
                .AddSingleton<OrderQuery>()
                .AddSingleton<OrderSchema>()
                .AddSingleton<OrderCreateInputType>()
                .AddSingleton<OrderMutation>()
                .AddSingleton<OrderSubscription>()
                .AddSingleton<OrderEventType>()
                .AddSingleton<IOrderEventService, OrderEventService>()
                .AddSingleton<IDependencyResolver>(
                    c => new FuncDependencyResolver(type => c.GetRequiredService(type)));

            services.AddGraphQL(new GraphQLOptions
                {
                    EnableMetrics = true,
                    ExposeExceptions = true
                })
                .AddWebSockets()
                .AddDataLoader();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseWebSockets();
            app.UseGraphQLWebSockets<OrderSchema>("/graphql");
            app.UseGraphiQl("/graphql");

            // use graphql-playground middleware at default url /ui/playground
            app.UseGraphQLPlayground(new GraphQLPlaygroundOptions());
            app.UseGraphQL<OrderSchema>("/graphql");


            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=GraphQL}/{action=Index}");
            });
        }
    }
}
